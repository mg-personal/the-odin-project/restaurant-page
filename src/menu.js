import { renderStructure } from './home';
import { renderAboutPage } from './about';

function renderMenuPage() {
    // content div
    var bodyContainer = document.getElementById("content");

    // Styling the website

    // Non-removable items

    // background
    document.body.style.backgroundImage =
        "url(/Users/maurogarcia/the_odin_project/restaurant-page/assets/frontpage-background-image.jpg)";
    document.body.style.backgroundSize = "cover";
    document.body.style.margin = "0";

    // navigation bar items
    var navBar = document.createElement('ul');
    var itemsDiv = document.createElement('div');
    var titleItem = document.createElement('li');
    var titleLink = document.createElement('a');
    var menuItem = document.createElement('li');
    var menuLink = document.createElement('a');
    var aboutItem = document.createElement('li');
    var aboutLink = document.createElement('a');

    bodyContainer.appendChild(navBar);
    navBar.appendChild(itemsDiv);
    itemsDiv.appendChild(titleItem);
    titleItem.appendChild(titleLink);
    itemsDiv.appendChild(menuItem);
    titleItem.appendChild(menuLink);
    itemsDiv.appendChild(aboutItem);
    titleItem.appendChild(aboutLink);

    titleLink.innerHTML = 'AGUAVIVA';
    menuLink.innerHTML = 'Menu';
    aboutLink.innerHTML = 'About';

    navBar.style.color = "white";
    navBar.style.listStyleType = "none";
    // navBar.style.backgroundColor = "#1a1b21";
    navBar.style.padding = "0";
    navBar.style.margin = "0";
    navBar.style.height = "50px";
    navBar.style.width = "100%";
    navBar.style.fontSize = "18px";
    navBar.style.fontFamily = "Arial";


    titleLink.style.display = "block";
    titleLink.style.marginLeft = "100px";
    titleLink.style.padding = "5px";
    titleLink.style.fontSize = "30px";
    titleLink.style.cssFloat = "left";
    titleLink.setAttribute("href", "#");
    titleLink.setAttribute("id", "title-link");
    titleLink.style.setProperty("text-decoration", "none");
    titleLink.style.color = "white";


    menuLink.style.display = "block";
    menuLink.style.padding = "15px";
    menuLink.style.cssFloat = "right";
    menuLink.setAttribute("href", "#");
    menuLink.setAttribute("id", "menu-link");
    menuLink.style.setProperty("text-decoration", "none");
    menuLink.style.color = "white";

    aboutLink.style.display = "block";
    aboutLink.style.padding = "15px";
    aboutLink.style.cssFloat = "right";
    aboutLink.setAttribute("href", "#");
    aboutLink.setAttribute("id", "about-link");
    aboutLink.style.setProperty("text-decoration", "none");
    aboutLink.style.color = "white";




    aboutLink.addEventListener('click', () => {
        while (bodyContainer.hasChildNodes()) {
            bodyContainer.removeChild(bodyContainer.firstChild);
        };
        renderAboutPage();
    })

    menuLink.addEventListener('click', () => {
        console.log("clicked menu");
    })

    titleLink.addEventListener('click', () => {
        console.log("clicked title/home");
        while (bodyContainer.hasChildNodes()) {
            bodyContainer.removeChild(bodyContainer.firstChild);
        };
        renderStructure();
    })
}

export { renderMenuPage }