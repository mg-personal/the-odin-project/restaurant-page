import { renderAboutPage } from './about';
import { renderMenuPage } from './menu';


function renderStructure() {
    // content div
    var bodyContainer = document.getElementById("content");

    // Styling the website

    // Non-removable items

    // background
    document.body.style.backgroundImage =
        "url(/Users/maurogarcia/the_odin_project/restaurant-page/assets/frontpage-background-image.jpg)";
    document.body.style.backgroundSize = "cover";
    document.body.style.margin = "0";

    // navigation bar items
    var navBar = document.createElement('ul');
    var itemsDiv = document.createElement('div');
    var titleItem = document.createElement('li');
    var titleLink = document.createElement('a');
    var menuItem = document.createElement('li');
    var menuLink = document.createElement('a');
    var aboutItem = document.createElement('li');
    var aboutLink = document.createElement('a');

    bodyContainer.appendChild(navBar);
    navBar.appendChild(itemsDiv);
    itemsDiv.appendChild(titleItem);
    titleItem.appendChild(titleLink);
    itemsDiv.appendChild(menuItem);
    titleItem.appendChild(menuLink);
    itemsDiv.appendChild(aboutItem);
    titleItem.appendChild(aboutLink);

    titleLink.innerHTML = 'AGUAVIVA';
    menuLink.innerHTML = 'Menu';
    aboutLink.innerHTML = 'About';

    navBar.style.color = "white";
    navBar.style.listStyleType = "none";
    // navBar.style.backgroundColor = "#1a1b21";
    navBar.style.padding = "0";
    navBar.style.margin = "0";
    navBar.style.height = "50px";
    navBar.style.width = "100%";
    navBar.style.fontSize = "18px";
    navBar.style.fontFamily = "Arial";


    titleLink.style.display = "block";
    titleLink.style.marginLeft = "100px";
    titleLink.style.padding = "5px";
    titleLink.style.fontSize = "30px";
    titleLink.style.cssFloat = "left";
    titleLink.setAttribute("href", "#");
    titleLink.setAttribute("id", "title-link");
    titleLink.style.setProperty("text-decoration", "none");
    titleLink.style.color = "white";


    menuLink.style.display = "block";
    menuLink.style.padding = "15px";
    menuLink.style.cssFloat = "right";
    menuLink.setAttribute("href", "#");
    menuLink.setAttribute("id", "menu-link");
    menuLink.style.setProperty("text-decoration", "none");
    menuLink.style.color = "white";

    aboutLink.style.display = "block";
    aboutLink.style.padding = "15px";
    aboutLink.style.cssFloat = "right";
    aboutLink.setAttribute("href", "#");
    aboutLink.setAttribute("id", "about-link");
    aboutLink.style.setProperty("text-decoration", "none");
    aboutLink.style.color = "white";
    // home content

    var pageContentDiv = document.createElement('div');
    var leftDiv = document.createElement('div');
    var rightDiv = document.createElement('div');
    var imgDiv = document.createElement('div');
    var welcomeDiv = document.createElement('div');
    var welcomeText = document.createElement('h1');

    bodyContainer.appendChild(pageContentDiv);
    pageContentDiv.appendChild(leftDiv);
    leftDiv.appendChild(imgDiv);
    pageContentDiv.appendChild(rightDiv);
    rightDiv.appendChild(welcomeDiv);
    welcomeDiv.appendChild(welcomeText);

    pageContentDiv.setAttribute("id", "page-content-div");

    leftDiv.style.height = "100%";
    leftDiv.style.width = "50%";
    leftDiv.style.position = "fixed";
    leftDiv.style.left = "0";
    // leftDiv.style.backgroundColor = "red";

    rightDiv.style.height = "100%";
    rightDiv.style.width = "50%";
    rightDiv.style.position = "fixed";
    rightDiv.style.right = "0";
    // rightDiv.style.backgroundColor = "blue";

    imgDiv.setAttribute("class", "img");
    // imgDiv.style.backgroundColor = "blue";
    imgDiv.style.backgroundImage = 
    "url(/Users/maurogarcia/the_odin_project/restaurant-page/assets/home-page-image.jpg)";
    imgDiv.style.backgroundSize = "cover";
    imgDiv.style.borderRadius = "8px";
    imgDiv.style.width = "700px";
    imgDiv.style.height = "500px";
    imgDiv.style.display = "block";
    imgDiv.style.marginTop = "25%";
    imgDiv.style.marginLeft = "auto";
    imgDiv.style.marginRight = "auto";

    welcomeDiv.setAttribute("class", "text");
    welcomeDiv.style.backgroundColor = "#1a1b21"
    // welcomeDiv.style.filter = "blur(5px)";
    welcomeDiv.style.borderRadius = "10px";
    welcomeDiv.style.width = "700px";
    welcomeDiv.style.height = "400px";
    welcomeDiv.style.display = "block";
    welcomeDiv.style.marginTop = "30%";
    welcomeDiv.style.marginLeft = "auto";
    welcomeDiv.style.marginRight = "auto";
    welcomeDiv.style.textAlign = "center";

    welcomeText.innerHTML = `Aguaviva specializes in the freshest 
    fish and seafood prepared with Latino –Caribbean  
    flair,  and  features  a  wonderful  oyster  and 
    ceviche  bar  in  front  of  the  open  kitchen.  
    In  2003,  Aguaviva  was named one of the  75 best 
    new restaurants  in the world by Conde Nast  Traveler  
    magazine.  A  variety  of  ceviches  are  offered,  served 
    with tostones of course, as well asseveral types of oysters, 
    which are   flown   in   daily,   as   are   the   fresh   
    Maine   lobsters.         The atmosphere at Aguaviva is 
    hip and refreshing, designed to feel as if you were dining 
    by the sea with its glowing blue and white décor.`;
    welcomeText.style.color = "white";
    welcomeText.style.fontSize = "25px";
    welcomeText.style.fontFamily = "'Mali', cursive";


    aboutLink.addEventListener('click', () => {
        console.log("clicked about");
        while (bodyContainer.hasChildNodes()) {
            bodyContainer.removeChild(bodyContainer.firstChild);
        };
        renderAboutPage();
    })
    
    menuLink.addEventListener('click', () => {
        while (bodyContainer.hasChildNodes()) {
            bodyContainer.removeChild(bodyContainer.firstChild);
        };
        renderMenuPage();
    })
    
    titleLink.addEventListener('click', () => {
        console.log("clicked title/home");
    })



}

export { renderStructure };