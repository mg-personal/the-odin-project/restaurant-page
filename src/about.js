import { renderStructure } from './home';
import { renderMenuPage } from './menu';

function renderAboutPage() {
    // content div
    var bodyContainer = document.getElementById("content");

    // Styling the website

    // Non-removable items

    // background
    document.body.style.backgroundImage =
        "url(/Users/maurogarcia/the_odin_project/restaurant-page/assets/frontpage-background-image.jpg)";
    document.body.style.backgroundSize = "cover";
    document.body.style.margin = "0";

    // navigation bar items
    var navBar = document.createElement('ul');
    var itemsDiv = document.createElement('div');
    var titleItem = document.createElement('li');
    var titleLink = document.createElement('a');
    var menuItem = document.createElement('li');
    var menuLink = document.createElement('a');
    var aboutItem = document.createElement('li');
    var aboutLink = document.createElement('a');

    bodyContainer.appendChild(navBar);
    navBar.appendChild(itemsDiv);
    itemsDiv.appendChild(titleItem);
    titleItem.appendChild(titleLink);
    itemsDiv.appendChild(menuItem);
    titleItem.appendChild(menuLink);
    itemsDiv.appendChild(aboutItem);
    titleItem.appendChild(aboutLink);

    titleLink.innerHTML = 'AGUAVIVA';
    menuLink.innerHTML = 'Menu';
    aboutLink.innerHTML = 'About';

    navBar.style.color = "white";
    navBar.style.listStyleType = "none";
    // navBar.style.backgroundColor = "#1a1b21";
    navBar.style.padding = "0";
    navBar.style.margin = "0";
    navBar.style.height = "50px";
    navBar.style.width = "100%";
    navBar.style.fontSize = "18px";
    navBar.style.fontFamily = "Arial";


    titleLink.style.display = "block";
    titleLink.style.marginLeft = "100px";
    titleLink.style.padding = "5px";
    titleLink.style.fontSize = "30px";
    titleLink.style.cssFloat = "left";
    titleLink.setAttribute("href", "#");
    titleLink.setAttribute("id", "title-link");
    titleLink.style.setProperty("text-decoration", "none");
    titleLink.style.color = "white";


    menuLink.style.display = "block";
    menuLink.style.padding = "15px";
    menuLink.style.cssFloat = "right";
    menuLink.setAttribute("href", "#");
    menuLink.setAttribute("id", "menu-link");
    menuLink.style.setProperty("text-decoration", "none");
    menuLink.style.color = "white";

    aboutLink.style.display = "block";
    aboutLink.style.padding = "15px";
    aboutLink.style.cssFloat = "right";
    aboutLink.setAttribute("href", "#");
    aboutLink.setAttribute("id", "about-link");
    aboutLink.style.setProperty("text-decoration", "none");
    aboutLink.style.color = "white";
    // home content

    var pageContentDiv = document.createElement('div');
    var leftDiv = document.createElement('div');
    var rightDiv = document.createElement('div');
    var infoDiv = document.createElement('div');
    var mapDiv = document.createElement('div');
    var locationText = document.createElement('h1');
    var openTimeText = document.createElement('h1');
    var phoneText = document.createElement('h1');

    bodyContainer.appendChild(pageContentDiv);
    pageContentDiv.appendChild(leftDiv);
    leftDiv.appendChild(mapDiv);
    pageContentDiv.appendChild(rightDiv);
    rightDiv.appendChild(infoDiv);
    infoDiv.appendChild(locationText);
    infoDiv.appendChild(openTimeText);
    infoDiv.appendChild(phoneText);

    pageContentDiv.setAttribute("id", "page-content-div");

    leftDiv.style.height = "100%";
    leftDiv.style.width = "50%";
    leftDiv.style.position = "fixed";
    leftDiv.style.left = "0";
    // leftDiv.style.backgroundColor = "red";

    rightDiv.style.height = "100%";
    rightDiv.style.width = "50%";
    rightDiv.style.position = "fixed";
    rightDiv.style.right = "0";
    // rightDiv.style.backgroundColor = "blue";

    mapDiv.setAttribute("id", "map");
    mapDiv.style.marginLeft = "25%";
    mapDiv.style.marginTop = "15%";
    mapDiv.style.height = "525px";
    mapDiv.style.width = "450px";

    var map = new ol.Map({
        target: 'map',
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            })
        ],
        view: new ol.View({
            center: ol.proj.fromLonLat([-58.826867, -27.466881]),
            zoom: 18
        })
    });


    // infoDiv.style.backgroundColor = "red";
    infoDiv.style.width = "75%";
    infoDiv.style.height = "50%";
    infoDiv.style.marginTop = "30%";
    infoDiv.style.marginLeft = "15%";
    infoDiv.style.color = "white";
    infoDiv.style.fontFamily = "'Mali', cursive";

    locationText.innerHTML = "Location: 9 de Julio 1875";
    openTimeText.innerHTML = "Working Hours: M - F 19hs - 00:30hs";
    phoneText.innerHTML = "+54 (9) 379 4435728";



    aboutLink.addEventListener('click', () => {
        while (bodyContainer.hasChildNodes()) {
            bodyContainer.removeChild(bodyContainer.firstChild);
        };
        console.log("clicked about");
    })

    menuLink.addEventListener('click', () => {
        while (bodyContainer.hasChildNodes()) {
            bodyContainer.removeChild(bodyContainer.firstChild);
        };
        renderMenuPage();
    })

    titleLink.addEventListener('click', () => {
        console.log("clicked title/home");
        while (bodyContainer.hasChildNodes()) {
            bodyContainer.removeChild(bodyContainer.firstChild);
        };
        renderStructure();
    })

}

export { renderAboutPage };
